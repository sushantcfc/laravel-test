<?php

namespace App\Http\Controllers;

use App\Models\Playlist;
use App\Traits\ApiResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\PlaylistResource;

use Validator;

class PlaylistController extends Controller
{
    use ApiResponse;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return PlayListResource::collection(Playlist::all());
    }

    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $validator = Validator::make($request->all(),[
            'title'=>'required',
            
        ]);
        if($validator->fails()){
            return $this->error(
               'Error Occured',422
            );
        }
        $playlist = PlayList::create([
            'title'=>$request->title,
            'user_id'=>Auth::user()->id
        ]);
        
            return $this->success([
                Playlist::latest()->first()
            ]);
    }

   
}
