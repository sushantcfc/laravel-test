<?php

namespace App\Http\Controllers;

use Validator;
use App\Models\Song;
use App\Traits\ApiResponse;
use Illuminate\Http\Request;
use App\Http\Resources\SongResource;

class SongController extends Controller
{

    use ApiResponse;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return SongResource::collection(Song::all());
    }

    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        

        $validator = Validator::make($request->all(),[
            'title'=>'required',
            'playlist'=>'required'
        ]);
        if($validator->fails()){
            return $this->error(
               'Error Occured',422
            );
        }

        $song = Song::create([
            'title'=>$request->title,
            'playlist_id'=>$request->playlist
        ]);
        
        return $this->success([
            Song::latest()->first()
        ]);
       
    }

    
}
