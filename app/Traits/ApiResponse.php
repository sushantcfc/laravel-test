<?php

namespace App\Traits;

use Carbon\Carbon;

// This trait will be used for any response we sent to clients.


trait ApiResponse
{
	
    // Return a success JSON response.
    
	protected function success($data, string $message = null, int $code = 200)
	{
		return response()->json([
			'status' => 'Success',
			'message' => $message,
			'data' => $data
		], $code);
	}

	
    //Return an error JSON response.
   
	protected function error(string $message = null, int $code, $data = null)
	{
		return response()->json([
			'status' => 'Error',
			'message' => $message,
			'data' => $data
		], $code);
	}

}