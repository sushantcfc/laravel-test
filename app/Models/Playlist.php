<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Playlist extends Model
{
    use HasFactory;
    protected $fillable = [
        'title',
        'user_id'
        
    ];


    public function songs()
    {
        return $this->hasMany('App\Models\Song');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
