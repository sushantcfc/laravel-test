<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SongController;
use App\Http\Controllers\PlaylistController;
use App\Http\Controllers\AuthenticationController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//register new user
Route::post('/register',[AuthenticationController::class, 'register']);

//SignIn user
Route::post('/login', [AuthenticationController::class, 'login']);

Route::group(['middleware' => ['auth:sanctum']], function () {
    Route::get('/playlists', [PlaylistController::class,'index']);
    Route::post('/playlist/save', [PlaylistController::class,'store']);
    Route::get('/songs', [SongController::class,'index']);
    Route::post('/song/save', [SongController::class,'store']);
    Route::post('/sign-out', [AuthenticationController::class, 'logout']);
});
