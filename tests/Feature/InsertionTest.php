<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\Song;
use App\Models\User;
use App\Models\Playlist;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class InsertionTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    

    public function test_playlist_can_be_added()
    {
        $user = User::factory()->create();
        $response = $this->actingAs($user)
         ->post('/api/playlist/save',[
            'title'=>"PlayList1",
            'user_id'=>$user->id
        ]);
        
        $response->assertStatus(200);
        $this->assertTrue(count(Playlist::all())>=1);
    }

    public function test_song_can_be_added()
    {
        $user = User::factory()->create();
        $playlist = Playlist::first();
        $response = $this->actingAs($user)
         ->post('/api/song/save',[
            'title'=>"PlayList1",
            'playlist'=>$playlist->id
        ]);
        
        $response->assertStatus(200);
        $this->assertTrue(count(Song::where('playlist_id','=',$playlist->id)->get())>=1);
    }
}
